/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natchamon.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class Product {
    
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espesso1" , 40, "1.png"));
        list.add(new Product(1, "Espesso2" , 40, "2.jpg"));
        list.add(new Product(1, "Espesso3" , 40, "3.jpg"));
        list.add(new Product(1, "Americano1" , 40, "1.png"));
        list.add(new Product(1, "Americano2" , 40, "2.jpg"));
        list.add(new Product(1, "Americano3" , 40, "3.jpg"));
        list.add(new Product(1, "Chayen1" , 40, "1.png"));
        list.add(new Product(1, "Chayen2" , 40, "2.jpg"));
        list.add(new Product(1, "Chayen3" , 40, "3.jpg"));
       
        return list;
        
    }
     
}
